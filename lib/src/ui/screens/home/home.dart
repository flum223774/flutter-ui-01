import 'package:flutter/material.dart';
import 'package:flutter_state_basic/src/ui/screens/home/home_wiget.dart';
import 'package:flutter_state_basic/src/ui/screens/home/tab_account.dart';
import 'package:flutter_state_basic/src/ui/screens/home/tab_cart.dart';
import 'package:flutter_state_basic/src/ui/screens/home/tab_home.dart';

class HomeSceen extends StatefulWidget {
  @override
  _HomeSceenState createState() => _HomeSceenState();
}

class _HomeSceenState extends State<HomeSceen> with HomeWidget {
  String _select = '1';
  List<String> choices = ['1', '2', '3'];
  int indexTab = 0;
  List<Widget> bottomTab = <Widget>[TabHome(), TabCart(), TabAccount()];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home page', style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.grey,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(
            Icons.menu,
            color: Colors.black,
          ),
          onPressed: () {},
        ),
        actions: <Widget>[
          PopupMenuButton<String>(
            color: Colors.red,
            onSelected: (seleted) {
              setState(() {
                _select = seleted;
              });
            },
            itemBuilder: (BuildContext context) {
              return choices.skip(2).map((String choice) {
                return PopupMenuItem<String>(
                    value: choice, child: Text(choice));
              }).toList();
            },
          ),
        ],
      ),
      body: bottomTab[indexTab],
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
          BottomNavigationBarItem(icon: Icon(Icons.shop), label: "Cart"),
          BottomNavigationBarItem(icon: Icon(Icons.ac_unit), label: "Account")
        ],
        currentIndex: indexTab,
        onTap: (index) {
          setState(() {
            indexTab = index;
          });
        },
      ),
    );
  }
}
